package np.com.hamrocollege.baseActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import np.com.hamrocollege.ProfileActivity;
import np.com.hamrocollege.R;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameField, passwordField;
    private DatabaseReference users;

    private FirebaseAuth mAuth;

    public static final String MY_PREFS = "prefs";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        DatabaseReference databaseRoot = FirebaseDatabase.getInstance().getReference();
        users = databaseRoot.child("users");
        /*Users Node in Database
        * users: {
        *   "kiran@bct71033": {
        *       "email" : "koirala.kiran1@gmail.com",
        *       "userType" : "student"
        *   },
        *   "amit@ct71006": {
        *       "email" : "amit6@gmail.com",
        *       "userType" : "staff"
        *    }
        *}
        * */

        usernameField = (EditText) findViewById(R.id.login_username);
        passwordField = (EditText) findViewById(R.id.login_password);

        Button signInButton = (Button) findViewById(R.id.login_signInButton);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Signing In...");

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null) {
            gotoHome(mAuth.getCurrentUser(), loadPreference() /* load user type */);
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateForm();
            }
        });
    }

    private void validateForm() {
        progressDialog.show();
        if(TextUtils.isEmpty(passwordField.getText().toString()) &&
                TextUtils.isEmpty((usernameField.getText().toString()))) {
            Toast.makeText(getApplicationContext(),
                    "Username and Password Required!",
                    Toast.LENGTH_LONG)
                    .show();
            progressDialog.dismiss();
        } else if(TextUtils.isEmpty(passwordField.getText().toString())) {
            Toast.makeText(getApplicationContext(),
                    "Password Required!",
                    Toast.LENGTH_LONG)
                    .show();
            progressDialog.dismiss();
        } else if(TextUtils.isEmpty((usernameField.getText().toString()))) {
            Toast.makeText(getApplicationContext(),
                    "Username Required!",
                    Toast.LENGTH_LONG)
                    .show();
            progressDialog.dismiss();
        } else {
            users.orderByKey().equalTo(usernameField.getText().toString()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.exists()) {
                        //Username doesnot exists in the list
                        Toast.makeText(getApplicationContext(),
                                "Invalid Username!",
                                Toast.LENGTH_LONG)
                                .show();
                        progressDialog.dismiss();
                    } else {
                        //Username Exists in the list
                        Toast.makeText(getApplicationContext(),
                                "Validation Passed!",
                                Toast.LENGTH_LONG)
                                .show();
                        signIn(dataSnapshot, usernameField.getText().toString());
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(),
                            "Failed to read value!",
                            Toast.LENGTH_LONG)
                            .show();
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void signIn(DataSnapshot dataSnapshot, String username) {
        //TODO: Show Progress Dialog

        String mEmail = dataSnapshot.child(username).child("email").getValue(String.class);
        String mPassword = passwordField.getText().toString();
        savePreference(dataSnapshot.child(username).child("userType").getValue(String.class));

        // start sign in with email
        if(mEmail != null) {
            mAuth.signInWithEmailAndPassword(mEmail, mPassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                //Sign in success and go to home
                                Toast.makeText(getApplicationContext(),
                                        "Authentication Successful!",
                                        Toast.LENGTH_LONG)
                                        .show();
                                FirebaseUser user = mAuth.getCurrentUser();
                                gotoHome(user, loadPreference());
                            } else {
                                //Sign in failed. So, display message to the user
                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                        Toast.LENGTH_LONG)
                                        .show();
                                progressDialog.dismiss();
                            }
                            // TODO: hide progress bar
                        }
                    });
        }

    }


    private void gotoHome(FirebaseUser user, String userType) {
        //TODO: hide progress bar
        if(user != null) {
            startActivity(new Intent(LoginActivity.this, ProfileActivity.class).putExtra("userType", userType));
            progressDialog.dismiss();
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "Not Signed In!",
                    Toast.LENGTH_LONG)
                    .show();
            progressDialog.dismiss();
        }
    }

    private void savePreference(String preference) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS, MODE_PRIVATE).edit();
        editor.putString("userType", preference);
//        Toast.makeText(LoginActivity.this, "saved: " + preference, Toast.LENGTH_LONG).show();
        editor.apply();
    }
    private String loadPreference() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS, MODE_PRIVATE);
        String restoredText = prefs.getString("userType", null);
//        Toast.makeText(this,"Loaded Preference: " + restoredText, Toast.LENGTH_LONG).show();
        return restoredText;
    }
}


