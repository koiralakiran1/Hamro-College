package np.com.hamrocollege.baseActivities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import np.com.hamrocollege.R;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        final Intent loginActivityIntent = new Intent(this, LoginActivity.class);

        Handler handler=new Handler();
        Runnable r=new Runnable() {
            public void run() {

                startActivity(loginActivityIntent);
                finish();
            }
        };
        handler.postDelayed(r, 2000);

    }

}
