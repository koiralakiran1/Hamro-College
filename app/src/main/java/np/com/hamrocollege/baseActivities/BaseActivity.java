package np.com.hamrocollege.baseActivities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import np.com.hamrocollege.R;

public class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    protected ActionBarDrawerToggle actionBarDrawerToggle;
    protected NavigationView navigationView;
    protected ActionBar actionBar;
    protected ImageButton btnNavigationDrawerBack, btnNavigationDrawerSettings;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        btnNavigationDrawerBack = (ImageButton) navigationView.getHeaderView(0).findViewById(R.id.navigation_drawer_header_back_button);
        btnNavigationDrawerSettings = (ImageButton) navigationView.getHeaderView(0).findViewById(R.id.navigation_drawer_header_settings_button);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
//        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        initializeOnClickListeners();
    }

    private void initializeOnClickListeners() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
//                    case R.id.nav_menu_gotoListLayoutActivity:
//                        Intent gotoListLayoutActivity = new Intent(getApplicationContext(), ListLayoutActivity.class);
//                        startActivity(gotoListLayoutActivity);
//                        break;
//                    case R.id.nav_menu_gotoRecyclerViewActivity:
//                        Intent gotoRecyclerViewActivity = new Intent(getApplicationContext(), RecyclerViewActivity.class);
//                        startActivity(gotoRecyclerViewActivity);
//                        break;
//                    case R.id.nav_menu_gotoMainActivity:
//                        Intent gotoMainActivity = new Intent(getApplicationContext(), MainActivity.class);
//                        startActivity(gotoMainActivity);
//                        break;
//
//                    case R.id.nav_menu_exit:
//                        Intent gotoFragmentActivity = new Intent(getApplicationContext(), FragmentActivity.class);
//                        startActivity(gotoFragmentActivity);
//                        break;
//                    default:
//                        break;

                }
                drawerLayout.closeDrawers();
                return true;
            }
        });
        btnNavigationDrawerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                //drawerLayout.closeDrawers();
            }
        });

        btnNavigationDrawerSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.inflateMenu(R.menu.toolbar_menu);
//        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                return onOptionsItemSelected(item);
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            //drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.toolbar_menu_about_us:
//                Toast.makeText(this, "About Us", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.toolbar_menu_exit:
//                Toast.makeText(this, "Exit", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.toolbar_menu_help:
//                Toast.makeText(this, "Help", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.toolbar_menu_settings:
//                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
//                return true;
////            case android.R.id.home:
////                mDrawerLayout.openDrawer(GravityCompat.START);
////                return true;
//            default:
//                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
