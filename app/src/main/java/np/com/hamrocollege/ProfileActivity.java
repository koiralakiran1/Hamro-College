package np.com.hamrocollege;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import np.com.hamrocollege.baseActivities.BaseActivity;
import np.com.hamrocollege.baseActivities.LoginActivity;

public class ProfileActivity extends BaseActivity {

    private FirebaseAuth fAuth;
    private FirebaseUser fCurrentUser;
    private DatabaseReference fDatabaseRoot, fStudents, fStaffs, fUserData;

    private TextView vNavigationDrawerProfileName,
                        vProfileHeaderProfileName, vProfileHeaderUserType, vProfileHeaderContact,
                        vProfileHeaderRollNumber, vProfileHeaderYear, vProfileHeaderFaculty;
    private View vNavigationDrawer;
//    private ImageView vNavImage;
//    private Button vprofileMessageButton;
    private FloatingActionButton btnChatFab;

    private String mProfileName, mRollNumber, mUserType, mYear, mFaculty, mContacts, mEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
        //        mNavigationView.getMenu().getItem(3).setChecked(true);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            findViewById(R.id.profile_header_fake_shadow).setVisibility(View.GONE);
        }

        mUserType = getIntent().getExtras().getString("userType");

        progressDialog.setTitle("Loading Data..");
        progressDialog.show();
        instantiateViews();
        instantiateFirebaseElements();
        putDetails();
        progressDialog.dismiss();
        initializeOnClickListeners();
    }

    private void initializeOnClickListeners() {
        btnChatFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void instantiateViews() {

        btnChatFab = (FloatingActionButton) findViewById(R.id.fab_button_chat);

        //navigationView is a navigation drawer instantiated in BaseActivity
        vNavigationDrawer = navigationView.getHeaderView(0);
        vNavigationDrawerProfileName = (TextView) vNavigationDrawer.findViewById(R.id.navigation_drawer_header_profileName);

        vProfileHeaderProfileName = (TextView) findViewById(R.id.profile_header_profileName);
        vProfileHeaderUserType = (TextView) findViewById(R.id.profile_header_userType);
        vProfileHeaderContact = (TextView) findViewById(R.id.profile_header_contact);
        vProfileHeaderRollNumber = (TextView) findViewById(R.id.profile_header_roll_no);
        vProfileHeaderYear = (TextView) findViewById(R.id.profile_header_year);
        vProfileHeaderFaculty = (TextView) findViewById(R.id.profile_header_faculty);
    }


    private void instantiateFirebaseElements(){
        fAuth = FirebaseAuth.getInstance();
        if(fAuth.getCurrentUser() == null) {
            Toast.makeText(this, "Not Signed In!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            finish();
        }
        fCurrentUser = fAuth.getCurrentUser();
        fDatabaseRoot = FirebaseDatabase.getInstance().getReference();
        fStudents = fDatabaseRoot.child("students");
        fStaffs = fDatabaseRoot.child("staffs");
    }


    private void putDetails() {
        final String uId = fCurrentUser.getUid();

        //mUserType comes from intent of LoginActivity
        if (mUserType.equals("student")) {
            fUserData = fStudents.child(uId);
        } else if (mUserType.equals("staff")) {
            fUserData = fStaffs.child(uId);
        } else {
            Toast.makeText(ProfileActivity.this, uId + "Not staff or student", Toast.LENGTH_LONG).show();
            finish();
        }

        fUserData.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Toast.makeText(ProfileActivity.this, "Student"+uId, Toast.LENGTH_LONG).show();

                mEmail = dataSnapshot.child("email").getValue(String.class);
                mProfileName = dataSnapshot.child("fullName").getValue(String.class);
                vNavigationDrawerProfileName.setText(mProfileName);
                vProfileHeaderProfileName.setText(mProfileName);

                mFaculty = dataSnapshot.child("faculty").getValue(String.class);
                vProfileHeaderFaculty.setText(mFaculty);

                mContacts = dataSnapshot.child("contactNumber").getValue(String.class);
                vProfileHeaderContact.setText(mContacts);

                vProfileHeaderUserType.setText(mUserType);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, "Student Error: " + databaseError.getCode(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            fAuth.signOut();
            Toast.makeText(this, "Signed Out", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
//        mNavigationView.getMenu().getItem(3).setChecked(true);
        super.onResume();
    }
}
