# Hamro College 

Hamro College is an android application built as a Major Project for the completion of our Bachelor's Degree in Computer Engineering. The main objective around building this application is to provide a seamless interface for college students and staffs to interact with each other and do their daily college activities from the app at the same time.

### Contributors
- [Amit Maharjan](https://www.facebook.com/amit.maharjan.982)
- [Ashish Ghimire](https://github.com/deashish)
- [Ashwin Neupane](https://github.com/ashwin101)
- [Kiran Koirala](https://github.com/koiralakiran1)
